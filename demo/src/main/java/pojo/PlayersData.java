package pojo;

public class PlayersData {
    private String playername;
    private String playerteam;
    private int jerNo;
    public String getPlayername() {
        return playername;
    }
    public void setPlayername(String playername) {
        this.playername = playername;
    }
    public String getPlayerteam() {
        return playerteam;
    }
    public void setPlayerteam(String playerteam) {
        this.playerteam = playerteam;
    }
    public int getJerNo() {
        return jerNo;
    }
    public void setJerNo(int jerNo) {
        this.jerNo = jerNo;
    }

}
